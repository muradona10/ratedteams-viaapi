package com.md.ratedteams.teamcatalog.controller;
import com.md.ratedteams.teamcatalog.model.Team;
import com.md.ratedteams.teamcatalog.model.UserRatings;
import com.md.ratedteams.teamcatalog.service.TeamInfoService;
import com.md.ratedteams.teamcatalog.service.UserRatingsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/teamcatalog")
public class TeamController {

    @Autowired
    private UserRatingsService userRatingsService;

    @Autowired
    private TeamInfoService teamInfoService;

    @GetMapping("/")
    public String home(){
        return "index";
    }

    @GetMapping("/ratedteams")
    public String getRatedTeams(Model model){

        //Get Users Rated Teams
        UserRatings ratings = userRatingsService.getUserRating();

        //for each rating data get actual information from API
        List<Team> teams = ratings.getRatings().stream().map(rating -> {
            Team team = teamInfoService.getTeamInfo(rating.getTeamName());
            team.setRating(rating.getRating());
            return team;
        }).collect(Collectors.toList());

        model.addAttribute("teams", teams);
        return "teams";
    }


}
