package com.md.ratedteams.teamcatalog.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Team {

    private Integer team_id;
    private String name;
    private String code;
    private String logo;
    private String country;
    private Integer founded;
    private String venue_name;
    private String venue_surface;
    private String venue_address;
    private String venue_city;
    private Integer venue_capacity;
    private Integer rating;

}
