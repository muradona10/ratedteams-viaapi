package com.md.ratedteams.userrating.repository;

import com.md.ratedteams.userrating.model.Rating;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.List;

@Slf4j
@Repository
public class DataLoader {

    @PostConstruct
    public List<Rating> dataLoad(){

        log.info("Post Construct DB load Operation");

        return Arrays.asList(
                new Rating("Fenerbahce", 5),
                new Rating("Galatasaray", 2),
                new Rating("Besiktas", 3),
                new Rating("Trabzonspor", 1),
                new Rating("Manchester United", 5),
                new Rating("Chelsea", 3),
                new Rating("Liverpool", 3),
                new Rating("Arsenal", 4),
                new Rating("Manchester City", 3),
                new Rating("Leicester", 4),
                new Rating("Real Madrid", 4),
                new Rating("Atletico Madrid", 4),
                new Rating("Barcelona", 3),
                new Rating("Juventus", 4),
                new Rating("Milan", 3),
                new Rating("Paris Saint Germain", 3),
                new Rating("Lyon", 3),
                new Rating("Bayern Munich", 4));

    }

}
