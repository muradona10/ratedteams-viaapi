package com.md.ratedteams.userrating.controller;

import com.md.ratedteams.userrating.model.Rating;
import com.md.ratedteams.userrating.model.UserRatings;
import com.md.ratedteams.userrating.repository.DataLoader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/api/v1/")
public class RatingController {

    @Autowired
    private DataLoader dataLoader;

    @GetMapping("/userratings")
    public UserRatings getRatings(){
        return new UserRatings(dataLoader.dataLoad());
    }

}
