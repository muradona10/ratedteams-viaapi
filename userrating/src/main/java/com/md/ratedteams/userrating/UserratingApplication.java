package com.md.ratedteams.userrating;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class UserratingApplication {

	public static void main(String[] args) {
		SpringApplication.run(UserratingApplication.class, args);
	}

}
