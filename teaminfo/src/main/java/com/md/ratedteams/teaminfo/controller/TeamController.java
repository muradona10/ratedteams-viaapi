package com.md.ratedteams.teaminfo.controller;

import com.md.ratedteams.teaminfo.model.Team;
import com.md.ratedteams.teaminfo.model.TeamData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("/api/v1/")
public class TeamController {

    @Value("${api.key}")
    private String apiKey;

    @Autowired
    private RestTemplate restTemplate;

    public static final String API_URL = "https://api-football-v1.p.rapidapi.com/v2/teams/search/";

    @GetMapping("/team/{teamName}")
    public Team getTeam(@PathVariable("teamName") String teamName){

        HttpHeaders headers = new HttpHeaders();
        headers.set("x-rapidapi-key", apiKey);
        HttpEntity<String> headerEntity = new HttpEntity<>(headers);

        ResponseEntity<TeamData> response = restTemplate.exchange(API_URL + teamName, HttpMethod.GET, headerEntity, TeamData.class);
        return response.getBody().getApi().getTeams().get(0);
    }

}
