package com.md.ratedteams.teaminfo.model;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class Api {

    private Integer results;
    private List<Team> teams = new ArrayList<>();

}
